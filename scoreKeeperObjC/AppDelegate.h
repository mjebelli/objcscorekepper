//
//  AppDelegate.h
//  scoreKeeperObjC
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۲/۳.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

