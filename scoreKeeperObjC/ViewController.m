//
//  ViewController.m
//  scoreKeeperObjC
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۲/۳.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    UIView* view = [UIView new];
    view.frame = CGRectZero;
    view.backgroundColor = [UIColor redColor];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view];
    
    NSDictionary* dictionary = NSDictionaryOfVariableBindings(view);
    NSDictionary* metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-100-|" options:0 metrics:metrics views:dictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-100-|" options:0 metrics:metrics views:dictionary]];
    
    
    
    
    UIView* view2 = [UIView new];
    view2.frame = CGRectZero;
    view2.backgroundColor = [UIColor blueColor];
    view2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view2];
    
    
    NSDictionary* dictionary2 = NSDictionaryOfVariableBindings(view2);
    NSDictionary* metrics2 = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-200-[view2]-0-|" options:0 metrics:metrics2 views:dictionary2]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view2]-100-|" options:0 metrics:metrics2 views:dictionary2]];
    
    
    
    
    
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, 250, 300, 50)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    label.text = @"0";
    [UIFont systemFontOfSize:30];
    [self.view addSubview:label];
    
    
    
    
    
    
    
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"Press Me" forState:UIControlStateNormal];
    [button sizeToFit];
    button.center = CGPointMake(200, 700);
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(UILabel:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
