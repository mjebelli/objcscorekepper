//
//  main.m
//  scoreKeeperObjC
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۲/۳.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
